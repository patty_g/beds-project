import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

fetch('//localhost:8080/beds/list')
  .then( response => response.text() )
  .then( text => console.log("we got a response!!",text))

class App extends Component {
  state = { beds_list:[] }
  async componentDidMount(){
    try{
    const response = await fetch('//localhost:8080/beds/list')
    const data = await response.json()
    this.setState({beds_list:data})
  }catch(err){
    console.log(err)
  }}
  render() {
    const { beds_list } = this.state
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          { beds_list.map( contact => 
      <div key={bed.id}>
        <p>{bed.id} -  {bed.size} - {bed.type}</p>
      </div>
  )
          }
          <p>{beds_list}</p>
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
