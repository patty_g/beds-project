import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
  
  /**
   * retrieves the contacts from the database
   */
  const getBedsList = async () => {
    const rows = await db.all("SELECT beds_id AS id, size, type FROM beds")
    return rows
  }

  const controller = {
    getBedsList
  }

  return controller
}

export default initializeDatabase
