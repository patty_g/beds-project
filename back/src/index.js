import app from './app'
import initializeDatabase from './db'

const start = async () => {
  const controller = await initializeDatabase()
  app.get('/', (req, res) => res.send("ok"));

  app.get('/beds/list', async (req, res) => {
    const beds_list = await controller.getBedsList()
    res.json(beds_list)
  })
  
  app.listen(8080, () => console.log('server listening on port 8080'))
}
start();

